# Demo-Cordova_Angular

Demo with Cordova and Angular

概述：
利用Cordova框架做的跨平台HybridApp应用，前端基于AngularJS的IonicV1做界面交互，在iOS和Android手机上运行实现UI统一。(WinPhone等其它平台系统的手机还没试)

简介：
用了基于AngularJS的IonicV1版本，调用其模块库搭建了流行的底部面板分页式界面。
头部；左边显示回退链接，中间展示页面名称，右边有功能菜单按钮。
主体；可上下滚动加载的面板，根据页面跳转显示相应内容。
底部；有聊天和列表和分享和设置的Tab，点击不同Tab时在主体显示相应内容。点击聊天Tab能进去看到对话信息，懒加载分页显示。

总结：
做这个Demo的主要目的在于了解Cordova框架的跨平台开发模式，它的特点就是利用原生代码封装好了跟H5交互的组件。其优点在于通过H5做界面实现了众多系统手机UI的统一，缺点可能就是对于性能较低的手机渲染H5时不够流畅。
